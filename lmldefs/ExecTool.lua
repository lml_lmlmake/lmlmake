return Tool {
    type = "exec",
    srcName = nil,
    tgtName = nil,
    onlyIf = nil,
    cmd = nil,
    run = function(self,srcDir,tgtDir)
        assert(self.cmd,"ExecTool: cmd must be given")
        srcDir,tgtDir = self:sanitizeDirs(srcDir,tgtDir)
        local cmd = self.cmd
        cmd = cmd:gsub("%${srcDir}",srcDir)
        cmd = cmd:gsub("%${tgtDir}",tgtDir)
        cmd = cmd:gsub("%${srcName}",self.srcName or "")
        cmd = cmd:gsub("%${tgtName}",self.tgtName or "")
        if not self.onlyIf or self.onlyIf(srcDir,self.srcName,
            tgtDir,self.tgtName) then
            os.execute(cmd)
        end
    end,
    
    TGTEXISTS = function(srcDir,srcName,tgtDir,tgtName)
        return (lfs.attributes(tgtDir.."/"..tgtName) and true) or false
    end,
    
    SRCNEWER = function(srcDir,srcName,tgtDir,tgtName)
        return (lfs.attributes(srcDir.."/"..srcName,"modification") >
            lfs.attributes(tgtDir.."/"..tgtName,"modification"))
    end,
    
    AND = function(f1,f2)
        return function(sD,sN,tD,tN)
            return (f1(sD,sN,tD,tN) and f2(sD,sN,tD,tN))
        end
    end,
    
    OR = function(f1,f2)
        return function(sD,sN,tD,tN)
            return (f1(sD,sN,tD,tN) or f2(sD,sN,tD,tN))
        end
    end,
    
    NOT = function(f1)
        return function(sD,sN,tD,tN)
            return (not f1(sD,sN,tD,tN))
        end
    end
}
