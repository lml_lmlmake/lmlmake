return Tool {
    type = "lml",
    srcName = nil,
    dir = nil,
    objName = nil,
    resObject = nil,
    setupEnv = nil,
    afterConstr = nil,
    run = function(self,srcDir,tgtDir,opts,...)
        srcDir,tgtDir = self:sanitizeDirs(srcDir,tgtDir)
        local env = (self.setupEnv and self.setupEnv(srcDir,tgtDir))
            or {}
        local obj
        if self.srcName then
            obj = loadLmlFile(srcDir.."/"..self.srcName,env)
        elseif self.dir then
            obj = includeDir(srcDir.."/"..self.dir,env)[self.objName]
        else
            error("LMLTool: Either dir or srcName bust be defined.")
        end
        if self.afterConstr then
            self.afterConstr(self,obj)
        end
        
        --allow child tools to append their results to our result
        --object; our result function (e.g., obj.result) can use these
        --child result objects (and possibly their functions) to
        --construct a sensible output
        local iObj = #obj
        local append = function(resObj)
            iObj = iObj + 1
            obj[iObj] = resObj
        end
        for _,child in ipairs(self) do
            child:run(srcDir,tgtDir,{append=append},...)
        end

        self.resObject = obj
        --if we got an append function from upward Tools, we append
        --our result object:
        if opts and opts.append then
            opts.append(obj)
        end
    end
}
