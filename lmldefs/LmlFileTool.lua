return LmlTool {
    --If tgtName is a string, a single file is written,
    --if it is a function, it is expected to generate a sequence
    --of file names.
    tgtName = nil,
    resPropName = "result",
    maxMulti = 1000,
    doWrite = true,
    write = function(self,result,tgtDir,tgt)
        assert(type(result)=="string",tostring(result)..
               " not a string")
        local f = io.open(tgtDir.."/"..tgt,"w")
        f:write(result)
        f:flush()
        f:close()
    end,
    
    run = function(self,srcDir,tgtDir,...)
        srcDir,tgtDir = self:sanitizeDirs(srcDir,tgtDir)
        LmlTool.run(self,srcDir,tgtDir,...)
        if not self.doWrite or not self.tgtName or tostring(self.tgtName) == "" then
            return
        end
        local obj = self.resObject
        local res = obj[self.resPropName]
        if type(self.tgtName) == "function" then
            assert(type(res) == "function","LmlFileTool: Multi output requires result function.")
            local nf = 1
            local resNf = res(obj,nf)
            while resNf and nf < self.maxMulti do
                local tgt = self.tgtName(nf)
                self:write(resNf,tgtDir,tgt)
                nf = nf + 1
                resNf = res(obj,nf)
            end
            return
        end
        if type(res) == "function" then
            res = res(obj)
        end
        self:write(res,tgtDir,self.tgtName)
    end
}
