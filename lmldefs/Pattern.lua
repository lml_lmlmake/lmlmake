return Multi {
    patterns = {},
    inverseOrder = false,
    -- one pattern = {namepat,fordirs,tool}
    unfold = function(self,dir)
        local entries,i = {},0
        for el in lfs.dir(dir) do
            i=i+1
            entries[i] = el
        end
        local comp
        if self.inverseOrder then
            comp = function(a,b) return a > b end
        end
        table.sort(entries,comp)
        for _,el in ipairs(entries) do
            local isdir = lfs.attributes(dir..el,"mode")=="directory"
            for _,pat in ipairs(self.patterns) do
                local namepat,fordirs,tool = table.unpack(pat)
                if fordirs ~= isdir then
                    goto continue
                end
                if namepat ~= ".*" and 
                        not el:find("^"..namepat.."$") then
                    goto continue
                end
                if tool == "recurse" then
                    local obj = Dir {
                        name = el,
                        Pattern {
                            patterns = patterns
                        }
                    }
                    table.insert(self,obj)
                else
                    local obj = tool {srcName = el}
                    table.insert(self,obj)
                end
                break
                ::continue::
            end
        end
    end,
    run = function(self,srcDir,tgtDir,...)
        self:unfold(srcDir)
        for _,child in ipairs(self) do
            child:run(srcDir,tgtDir,...)
        end
    end
}
