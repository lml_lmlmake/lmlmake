return Tool {
    type = "copy",
    srcName = nil,
    tgtName = nil,
    chunksize = 1024,
    run = function(self,srcDir,tgtDir)
        assert(self.srcName,"CopyTool: srcName must be given")
        local chunksize = self.chunksize
        srcDir,tgtDir = self:sanitizeDirs(srcDir,tgtDir)
        self.tgtName = self.tgtName or self.srcName
        local t = io.open(tgtDir.."/"..self.tgtName,"w")
        local s = io.open(srcDir.."/"..self.srcName,"r")
        assert(s,"CopyTool: could not open file"..srcDir.."/"..self.srcName.." for reading.")
        assert(t,"CopyTool: could not open file"..tgtDir.."/"..self.tgtName.." for writing.")
        local chunk = s:read(chunksize)
        while chunk do
            t:write(chunk)
            chunk = s:read(chunksize)
        end
        t:flush()
        t:close()
        lfs.touch(tgtDir.."/"..self.tgtName,
            lfs.attributes(srcDir.."/"..self.srcName,"modification"))
    end
}
