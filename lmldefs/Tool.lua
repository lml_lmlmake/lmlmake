return copyOI(Object) {
    type = "none",
    srcDir = nil,
    tgtDir = nil,
    passDirs = function(obj)
        local srcDir,tgtDir = obj.srcDir,tgtDir
        if not srcDir and not tgtDir then
            return
        end
        for _,child in ipairs(obj) do
            local modfd = false
            if not child.srcDir and srcDir then
                child.srcDir = srcDir
                modfd = true
            end
            if not child.tgtDir and tgtDir then
                child.tgtDir = tgtDir
                modfd = true
            end
            if modfd then
                child:passDirs()
            end
        end
    end,
    sanitizeDirs = function(self,srcDir,tgtDir)
        if self.srcDir then
            srcDir = srcDir .. "/" .. self.srcDir
        end
        if self.tgtDir then
            tgtDir = tgtDir .. "/" .. self.tgtDir
        end
        --TODO: remove multi / , . and  .. parts
        return srcDir,tgtDir
    end,
    run = function(self,srcDir,tgtDir)
        srcDir,tgtDir = self:sanitizeDirs(srcDir,tgtDir)
        -- ...
        --if defined srcDir and tgtDir shall end with a slash
        --defaults to "", i.e, paths prefixed with either of
        --them are global
    end
}
