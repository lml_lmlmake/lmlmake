# lmlmake
Make utility based on [LML](https://gitlab.com/lml_lmlmake/lml).
LML should be already in your package path or at the same level as the
lmlmake **folder** (i.e., `../lml` seen from the `lmlmake` script).

Note that LML is not a Makefile generator like cmake but is invoked
similar to make itself. It expects a file called `LMakefile` it the
current working directory.

`lmlmake` does not have *targets* as the main building block,
but *tools*. A tool can have child tools. For instance the tool `Dir`
changes the directory in both the source and the build (or target)
tree. Then it iterates over its children with these directories as 
`srcDir` and `tgtDir`. Every Tool has a `run` method that takes the
`srcDir` and `tgtDir` as an argument and possibly further arguments.
Those are either directly processed by the tool or passed on to the
`run` methods of its children. This is used in the Tests example. The
`Dir` tool passes any jobname given on the command line to the
`Choice` tool. Thus the target selection from usual make is not done
on the topmost level.

Note you can define your own tools by instantiating the prototypes
of lmlmake within a single LMakefile. E.g.:

    local MyDir = Dir {...}
    local ConvertToDos = ExecTool {...}
    
    return Choice {
        main = MyDir { ConvertToDos {tgt = "unix.txt"} },
        install = ...
    }



License
----
 * This software is licensed under GPLv3+, see each file header.
 * If you need a more permissive license (e.g. BSD-style),
 please contact the author.

