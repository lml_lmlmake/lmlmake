return Multi {
    type = "multi",
    name = nil,
    tgtName = nil,
    run = function(self,srcDir,tgtDir,...)
        local tgtName = self.tgtName or self.name
        assert(tgtName,"Dir: name or tgtName must be given")
        srcDir = srcDir .. ((self.name and ("/"..self.name)) or "")
        tgtDir = tgtDir .. ((tgtName~="" and ("/"..tgtName)) or "")
        lfs.mkdir(tgtDir)
        for _,child in ipairs(self) do
            self:doCommon(child)
            child:run(srcDir,tgtDir,...)
        end
    end
}
