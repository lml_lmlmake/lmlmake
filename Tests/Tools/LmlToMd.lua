return LmlFileTool {
    id = "tool",
    target = bind("tool").file:sub(1,-5) .. ".md",
    setupEnv = function(srcDir,tgtDir)
        return includeDirRel("LmlToMd",{
            srcDir=srcDir,
            tgtDir=tgtDir,
            ipairs=ipairs
        })
    end
}
