return Tool {
    type = "multi",
    file = nil,
    run = function(self,srcDir,tgtDir,...)
        assert(self.file,"LmlMake: file must be given")
        srcDir,tgtDir = self:sanitizeDirs(srcDir,tgtDir)
        local obj = loadLmlFile(srcDir..self.file,_ENV)
        obj:run(srcDir,tgtDir,...)
    end
}
