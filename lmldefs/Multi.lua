return Tool {
    type = "multi",
    common = nil,
    commonByTool = nil,
    doCommon = function(self,child)
        local comms = {self.common or {},self.commonByTool and self.commonByTool[protoname(child)]}
        for _,com in ipairs(comms) do
            if type(com) == "function" then
                    com(child)
            elseif type(com) == "table" then
                for k,v in pairs(com) do
                    child[k] = v
                end
            end
        end
    end,
    run = function(self,srcDir,tgtDir,...)
        for _,child in ipairs(self) do
            self:doCommon(child)
            child:run(srcDir,tgtDir,...)
        end
    end
}
