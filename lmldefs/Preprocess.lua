return Tool {
    type = "multi",
    run = function(self,srcDir,tgtDir,...)
        for _,child in ipairs(self) do
            child:run(srcDir,srcDir,...)
        end
    end
}
