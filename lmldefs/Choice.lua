return Tool {
    type = "choice",
    default = nil,
    run = function(self,srcDir,tgtDir,job,...)
        srcDir,tgtDir = self:sanitizeDirs(srcDir,tgtDir)
        self:runnosan(srcDir,tgtDir,job,...)
    end,
    runnosan = function(self,srcDir,tgtDir,job,...)
        job = job or self.default
        assert(job,"Choice: No jobname given and no default")
        local deps = self[job].deps
        if deps then
            for _,d in ipairs(deps) do
                self:runnosan(srcDir,tgtDir,d,...)
            end
        end
        self[job]:run(srcDir,tgtDir,...)
    end
}
